@echo off

IF "%ADP_DIR%" == "" (
  SET ADP_DIR=%TMP%
)

IF "%ADP_CLI_JAR%" == "" (
  SET ADP_CLI_JAR=adp-cli-latest.jar
)

IF NOT EXIST %ADP_DIR%\%ADP_CLI_JAR% (
  echo %ADP_CLI_JAR%
  curl -o %ADP_DIR%\%ADP_CLI_JAR% https://arbes-repository.s3.eu-central-1.amazonaws.com/dist/adp-cli/%ADP_CLI_JAR%
)

set AWS_PROFILE=arbes
java -jar %ADP_DIR%\%ADP_CLI_JAR% %*