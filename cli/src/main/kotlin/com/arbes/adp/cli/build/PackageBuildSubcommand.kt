package com.arbes.adp.cli.build

import com.arbes.adp.cli.process
import kotlinx.cli.Subcommand

object PackageBuildSubcommand : Subcommand(
    "package-build",
    "Build all packages from this ADP project via docker compose (ensure that adp-server-components are installed in ~/.m2/repository)"
) {

    override fun execute() {
        process("docker compose up --remove-orphans")
        process("docker compose down")
    }
}