package com.arbes.adp.cli

import java.io.BufferedReader
import java.io.File
import java.io.InputStreamReader
import kotlin.system.exitProcess

fun process(directory: File?, command: String, callback: ((String) -> Unit)): Int {
    println("[${directory ?: "."}]: $command")
    return ProcessBuilder(*command.split(' ').toTypedArray()).run {
        if (directory != null)
            directory(directory)
        redirectErrorStream(true)
        val proc = start()
        val input = BufferedReader(InputStreamReader(proc.inputStream))
        while (true) {
            val line = input.readLine() ?: break
            callback(line)
        }
        proc.waitFor()
    }
}

fun process(directory: File?, command: String, returnOutput: Boolean = false): List<String> {
    val output = mutableListOf<String>()
    val status = process(directory, command) {
        if (returnOutput)
            output += it
        else
            println(it)
    }
    if (status != 0)
        exitProcess(status)
    return output
}

fun process(command: String) = process(null, command)


