package com.arbes.adp.cli

import kotlinx.cli.Subcommand

object SeedDataSubcommand : Subcommand(
    "seed-data",
    "Seed initial data in specific deployment tier"
) {

    override fun execute() {
        sqlDatabases.forEach {
            process("./create-sql-database.sh adp $it")
        }
    }
}