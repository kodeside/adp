package com.arbes.adp.cli.terraform
import com.arbes.adp.cli.process
import kotlinx.cli.ArgType
import kotlinx.cli.default
import java.io.File

object TfWorkspaceSubcommand : TfAbstractSubcommand(
    "tf-workspace",
    "Set ADP workspace via Terraform (T3)"
) {
    private val namespace by option(ArgType.String, "name", "n", "Workspace name").default(com.arbes.adp.cli.namespace)

    override fun execute() {
        File(tfDir, "adp-ri").let {
            process(it, "terraform init")
            process(it, "terraform workspace new $namespace")
            process(it, "terraform apply")
        }
    }
}