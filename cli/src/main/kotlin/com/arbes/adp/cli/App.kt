package com.arbes.adp.cli

import com.arbes.adp.cli.terraform.*
import com.arbes.adp.cli.build.*
import com.arbes.adp.cli.docker.*
import com.arbes.adp.cli.kubernetes.*
import kotlinx.cli.ArgParser

object App {
    @JvmStatic
    fun main(args: Array<String>) {
        with(ArgParser("adp")) {
            subcommands(
                MvnInstallSubcommand,
                PackageBuildSubcommand,
                DockerBuildSubcommand,
                DockerUpSubcommand,
                DockerDownSubcommand,
                DockerStatusSubcommand,
                K8sInstallSubcommand,
                K8sUninstallSubcommand,
                K8sStatusSubcommand,
                TfCreateSubcommand,
                TfWorkspaceSubcommand,
                TfDestroySubcommand,
                TfStatusSubcommand,
                SeedDataSubcommand
            )
            println("ADP CLI Version $version")
            parse(if (args.isEmpty()) arrayOf("-h") else args)
        }
    }
}