package com.arbes.adp.cli.kubernetes

import com.arbes.adp.cli.*
import java.io.File

object K8sInstallSubcommand : ServiceSubcommand(
    "k8s-install",
    "Install ADP service(s) to Kubernetes cluster (T2/T3) (use kubectl config use-context or kubectx tool to select context first)"
) {
    override fun execute() {
        services.filter {
            k8sServices.contains(it.name)
        }.forEach {
            val chartFile = File(it, "deploy/Chart.txt")
            val chart = if (chartFile.exists())
                chartFile.readText()
            else
                "${it.path}/deploy"
            process("helm -n $namespace upgrade --install ${it.name} $chart -f shared/common/deploy/values-t2.yaml -f $it/deploy/values-t2.yaml --set baseDir=$baseDir --set name=${it.name}")
        }
    }
}