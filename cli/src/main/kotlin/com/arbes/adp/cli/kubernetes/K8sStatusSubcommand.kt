package com.arbes.adp.cli.kubernetes

import com.arbes.adp.cli.namespace
import com.arbes.adp.cli.process
import kotlinx.cli.Subcommand

object K8sStatusSubcommand : Subcommand(
    "k8s-status",
    "List installed service(s) in Kubernetes cluster (T2/T3)"
) {

    override fun execute() {
        process("helm -n $namespace list")
    }
}