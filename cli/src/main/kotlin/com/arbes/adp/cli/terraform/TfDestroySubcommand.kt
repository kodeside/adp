package com.arbes.adp.cli.terraform

import com.arbes.adp.cli.process
import java.io.File

object TfDestroySubcommand : TfAbstractSubcommand(
    "tf-destroy",
    "Uninstall ADP cloud infrastructure via Terraform (T3)"
) {

    override fun execute() {
        process(File(tfDir, "global_infrastructure"), "terraform destroy -auto-approve")
    }
}