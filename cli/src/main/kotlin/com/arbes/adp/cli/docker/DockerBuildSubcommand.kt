package com.arbes.adp.cli.docker

import com.arbes.adp.cli.ServiceSubcommand
import com.arbes.adp.cli.process

object DockerBuildSubcommand : ServiceSubcommand(
    "docker-build",
    "Build ADP service(s) Docker image(s)"
) {

    override fun execute() {
        println("Building docker images $services")
        services.forEach {
            process(it, "docker compose build")
        }
    }
}