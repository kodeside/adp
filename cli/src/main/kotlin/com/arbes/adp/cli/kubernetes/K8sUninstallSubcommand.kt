package com.arbes.adp.cli.kubernetes

import com.arbes.adp.cli.ServiceSubcommand
import com.arbes.adp.cli.k8sServices
import com.arbes.adp.cli.namespace
import com.arbes.adp.cli.process

object K8sUninstallSubcommand : ServiceSubcommand(
    "k8s-uninstall",
    "Uninstall service(s) from Kubernetes cluster"
) {
    override fun execute() {
        services.filter {
            k8sServices.contains(it.name)
        }.forEach {
            process("helm -n $namespace delete ${it.name}")
        }
    }
}