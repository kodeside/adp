package com.arbes.adp.cli.docker

import com.arbes.adp.cli.process
import kotlinx.cli.Subcommand

object DockerStatusSubcommand : Subcommand(
    "docker-status",
    "List running ADP service(s) in Docker (T1)"
) {
    override fun execute() {
        process("docker ps")
    }
}