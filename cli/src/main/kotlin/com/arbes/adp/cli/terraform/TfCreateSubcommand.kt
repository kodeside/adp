package com.arbes.adp.cli.terraform

import com.arbes.adp.cli.process
import java.io.File

object TfCreateSubcommand : TfAbstractSubcommand(
    "tf-create",
    "Create ADP cloud infrastructure via Terraform (T3)"
) {
    override fun execute() {
        File(tfDir, "global_infrastructure").let {
            if (cloudType == CloudType.AWS)
                process(it, "aws configure --profile arbes")
            process(it, "terraform init")
            process(it, "terraform apply -auto-approve")
            if (cloudType == CloudType.AWS)
                process(it, "aws eks update-kubeconfig --name ARBES-RI-DEV")
        }
    }
}