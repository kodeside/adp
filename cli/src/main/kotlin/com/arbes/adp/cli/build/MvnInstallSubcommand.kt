package com.arbes.adp.cli.build

import com.arbes.adp.cli.process
import com.arbes.adp.cli.scriptExt
import kotlinx.cli.Subcommand

object MvnInstallSubcommand : Subcommand(
    "mvn-install",
    "Build all Java packages using Maven"
) {

    override fun execute() {
        process("./mvn-install.$scriptExt -s .mvn/settings.xml -P adp-gitlab")
    }
}