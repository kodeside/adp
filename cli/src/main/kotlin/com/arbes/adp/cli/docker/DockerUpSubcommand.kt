package com.arbes.adp.cli.docker

import com.arbes.adp.cli.ServiceSubcommand
import com.arbes.adp.cli.dockerServices
import com.arbes.adp.cli.process
import java.io.File

object DockerUpSubcommand : ServiceSubcommand(
    "docker-up",
    "Run ADP service(s) in Docker (T1)"
) {
    override fun execute() {
        services.filter {
            dockerServices.contains(it.name)
        }.forEach {
            if (!File(it, "docker-compose.yaml").exists())
                println("Missing docker-compose.yaml for service $it")
            else
                process(it, "docker compose up -d")
        }
    }
}