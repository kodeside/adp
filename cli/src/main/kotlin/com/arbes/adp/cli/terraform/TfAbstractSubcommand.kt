package com.arbes.adp.cli.terraform

import kotlinx.cli.ArgType
import kotlinx.cli.Subcommand
import java.io.File

abstract class TfAbstractSubcommand(name: String, actionDescription: String) : Subcommand(name, actionDescription) {

    protected val cloudType by argument(ArgType.Choice<CloudType>(), "cloud")
    protected val tfDir by lazy {
        if (cloudType != CloudType.AWS)
            error("Unsupported cloud type: $cloudType")
        File("cli/src/main/resources/tf/${cloudType.name}").let {
            if (!it.exists()) {
                it//TODO extract & return tmp dir
            } else {
                it
            }
        }
    }
}
