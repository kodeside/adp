package com.arbes.adp.cli

import java.io.File

val isWindows get() = System.getProperty("os.name").lowercase().contains("win")
val scriptExt get() = if (isWindows) "cmd" else "sh"
val namespace get() = System.getenv("ADP_NAMESPACE") ?: "adp"
val baseDir: String get() = File(".").canonicalPath

val dockerServices get() = System.getenv("ADP_DOCKER_SERVICES")?.split(',') ?:
listOf("postgres", "keycloak", "graylog", "prometheus")

val k8sServices get() = System.getenv("ADP_K8S_SERVICES")?.split(',') ?:
listOf("postgres", "mongo", "elasticsearch", "keycloak", "graylog", "prometheus")

val sqlDatabases get() = System.getenv("ADP_SQL_DATABASES")?.split(',') ?:
listOf("keycloak")

val Any.version get() = String(javaClass.getResourceAsStream("/version.txt").readAllBytes())