package com.arbes.adp.cli.terraform

enum class CloudType {
    AWS,
    GCP,
    Azure,
}