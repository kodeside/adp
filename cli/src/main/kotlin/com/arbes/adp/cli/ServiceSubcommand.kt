package com.arbes.adp.cli

import kotlinx.cli.ArgType
import kotlinx.cli.ExperimentalCli
import kotlinx.cli.Subcommand
import java.io.File
import kotlin.system.exitProcess

open class ServiceSubcommand(name: String, description: String) : Subcommand(name, description) {

    private val serviceName by option(ArgType.String, "service", "s", "Service name")

    val services get() = File("services")
        .listFiles { file -> file.isDirectory }
        .filter { serviceName == null || serviceName == it.name }
        .ifEmpty {
            println("Invalid service specified ($serviceName)")
            exitProcess(0)
        }

    @ExperimentalCli
    override fun execute() {
        println(services)
    }

}