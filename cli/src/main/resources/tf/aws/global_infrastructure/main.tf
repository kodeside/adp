// Tags

module "global_tags" {
  source              = "../modules/tags"
  environment         = "global"
  git_repository      = "gitlab.com/arbes-technologies/adp-ri"
  data_classification = "internal"
}

module "environmental_tags" {
  source              = "../modules/tags"
  environment         = local.environment
  git_repository      = "gitlab.com/arbes-technologies/adp-ri"
  data_classification = "internal"
}

// Deploy main VPC and shared resources - subnets, security groups

module "main_vpc" {
  source      = "../modules/vpc"
  name        = upper(local.identifier)
  azs         = local.azs
  cidr_block  = "10.60.0.0/16"
  environment = local.environment
  tags        = module.global_tags.tags
}

// Global environment deployment (EKS)

module "kubernetes" {
  source             = "../modules/eks"
  cluster_name       = upper(local.identifier)
  public_access_api  = true
  private_subnet_ids = module.main_vpc.private_subnet_ids
  tags               = module.environmental_tags.tags

  depends_on = [
    module.kms_eks, module.main_vpc
  ]
}

// Global environment deployment (databases)

module "database_postgres" {
  source                  = "../modules/postgres"
  identifier              = local.identifier
  tags                    = module.environmental_tags.tags
  allowed_security_groups = [module.kubernetes.source_security_group_id]

  depends_on = [
    module.kms_rds, module.kubernetes, module.main_vpc
  ]
}

# module "database_aurora" {
#   source        = "../modules/rds_aurora"
#   cluster_name  = local.deployment_identifier // Will use K8s provider inside to store master password into kube-system namespace
#   instance_type = var.instance_type
# }
