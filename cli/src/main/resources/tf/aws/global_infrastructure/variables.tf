variable "environment" {
  default = "dev"
}

variable "use_case_name" {
  default = "arbes-ri"
}
