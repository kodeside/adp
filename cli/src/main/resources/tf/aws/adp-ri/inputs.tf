// Statefile from global environment - this will grant access to correct K8s and database cluster to create resources in

data "terraform_remote_state" "global" {
  backend = "s3"
  config = {
    bucket = "tfstates-arbes-dev"
    key    = "global_infrastructure/arbes-ri/dev.tfstate"
    region = "eu-central-1"
  }
}
