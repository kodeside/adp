Environment preparation and development
===
Infrastructure has two main parts - global infrastructure and application workspace.

## Global infrastructure
For future we assume single isolated AWS account being an environmental boundary. Each AWS account shall contain one VPC with basic configuration, one EKS cluster, one RDS database. In general, whole infrastructure lives strictly in private subnets, however at the time of writing this document there was no private connection (VPN or Direct Connection) available, thus we deploy EKS with public facing endpoint.

Statefile represents a deployment of configuration of single AWS account, all Terraform lives in `global_infrastructure` directory and responsibility of related modules should belong to SRE/DevOps team in the future.

Basic configuration of cluster wihde components (nginx-controller, external-dns and cert-manager) is handled by Helm charts.

It is generally assumed, that developers do not need to run Terraform in this directory, unless some global changes are necessary.

## Application workspace
Assuming **single** deployment of global infrastructure in AWS account, we'd like to enable **multiple** developers having their own workspaces, isolated deployments within. Code in `adp-ri` directory represent Kubernetes namespace living within Terraform workspace. Any developer can create it's own workspace = k8s namespace for later deployments of his application stack.

Future development will also contain creation of brand new database(s) and users within global RDS instance - this will require SSH tunnel from local machine via Kubernetes cluster for local work.

# Getting started
- get access to AWS account with user credentials, generate secret access key and access key id via IAM for your user (DO NOT COMMIT THESE CREDENTIALS TO GIT!)
- install AWS cli, Terraform version ~>1.2, Helm, Helmfile, Kubectl
- recommended to install also K9s or similar Kubernetes viewer for easier handling
- `aws configure --profile arbes` with your secret access key and access key id, region `eu-central-1`
- `export AWS_PROFILE=arbes` to make sure all of AWS related commands will be using correct profile credentials
- get credentials to you Kubeconfig via `aws eks update-kubeconfig --name arbes-ri-dev`

From this moment on, you should be able to run Terraform and access Kubernetes API.

# Creating new workspace
In directory `adp-ri`:
- `terraform init`
- `terraform workspace show` to see all existing workspaces
- `terraform workspace select *name*` to switch between desired workspaces
- `terraform workspace new *name*` to create brand new workspace
- `terraform plan` to see desired changes
- `terraform apply` to actually deploy changes

# Applying Helm(file)
- helm(file) --kube-context *your_context_name* -n adp-ri-*terraform_workspace*` should be enough for you to start deploying your Helm charts.