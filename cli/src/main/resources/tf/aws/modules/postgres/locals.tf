locals {
  identifier            = replace(var.identifier, "_", "-")
  pgaudit_log_parameter = 1
  log_error_verbosity   = var.log_error_verbosity == null ? "default" : "verbose"
  log_statement         = "ddl"

  # Engine version to load selective shared_preload_libraries
  postgres_major_list = split(".", var.postgres_version)
  postgres_major = join(
    ".",
    slice(
      local.postgres_major_list,
      0,
      length(local.postgres_major_list) - 1,
    ),
  )

  tags = merge(
    { Name = local.identifier },
    var.tags
  )
}
