// Master user password
resource "random_password" "main" {
  length  = 24
  special = false
}

// Select KMS key for storage encryption

data "aws_kms_key" "main" {
  key_id = "alias/arbes/RDS"
}

// Vanilla Postgres instance

resource "aws_db_instance" "main" {
  identifier                      = local.identifier
  allocated_storage               = var.storage_size
  storage_type                    = "gp2"
  db_subnet_group_name            = aws_db_subnet_group.global.name
  vpc_security_group_ids          = [aws_security_group.main.id]
  engine                          = "postgres"
  engine_version                  = var.postgres_version
  instance_class                  = var.db_instance_type
  username                        = var.username
  password                        = random_password.main.result
  skip_final_snapshot             = false
  backup_retention_period         = 5
  backup_window                   = "09:00-10:00"
  maintenance_window              = "Mon:07:00-Mon:08:00"
  port                            = 5432
  publicly_accessible             = false
  multi_az                        = var.multi_az
  storage_encrypted               = true
  kms_key_id                      = data.aws_kms_key.main.arn
  enabled_cloudwatch_logs_exports = var.enabled_cloudwatch_logs_exports
  performance_insights_enabled    = var.performance_insights_enabled
  apply_immediately               = true
  parameter_group_name            = aws_db_parameter_group.main.name

  copy_tags_to_snapshot     = true
  final_snapshot_identifier = join("-", [local.identifier, formatdate("YYYY-MM-DD-hh-mm", timestamp())])
  tags                      = local.tags

  lifecycle {
    ignore_changes = [storage_encrypted, kms_key_id]
  }
}

// Database parameters

resource "aws_db_parameter_group" "main" {
  name_prefix = local.identifier
  family      = var.param_group_family
  tags        = local.tags

  parameter {
    name  = "rds.log_retention_period"
    value = 24 * 60 * 7
  }

  parameter {
    name  = "log_min_duration_statement"
    value = var.log_min_duration_statement
  }

  parameter {
    name  = "log_connections"
    value = var.log_connections
  }

  parameter {
    name  = "log_disconnections"
    value = var.log_disconnections
  }

  parameter {
    name  = "log_statement"
    value = local.log_statement
  }

  parameter {
    // Can be pending-reboot or immediate, but immediate will reboot your db
    apply_method = "pending-reboot"
    name         = "shared_preload_libraries"
    value        = join(",", lookup(var.shared_preload_libraries, local.postgres_major))
  }

  parameter {
    apply_method = "pending-reboot"
    name         = "pgaudit.role"
    value        = var.pgaudit_role
  }

  parameter {
    apply_method = "immediate"
    name         = "password_encryption"
    value        = "scram-sha-256"
  }

  parameter {
    apply_method = "immediate"
    name         = "pgaudit.log_parameter"
    value        = local.pgaudit_log_parameter
  }

  parameter {
    apply_method = "immediate"
    name         = "log_error_verbosity"
    value        = local.log_error_verbosity
  }


  lifecycle {
    create_before_destroy = true
  }
}
