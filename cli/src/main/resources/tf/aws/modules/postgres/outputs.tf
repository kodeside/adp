// Password, username, DNS record

output "master_password" {
  value     = ""
  sensitive = true
}

output "master_username" {
  value = ""
}

output "database_url" {
  value = ""
}
