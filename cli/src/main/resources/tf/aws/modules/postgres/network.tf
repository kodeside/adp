
// VPC configuration

data "aws_subnet" "global" {
  tags = {
    Name = "Terraform Private 0" // This is merely hardcoded - it is fair to expect at least one subnet exists. Could be also passed in from VPC module
  }
}

data "aws_subnets" "global" { // Collect all private subnets from the VPC

  filter {
    name   = "vpc-id"
    values = [data.aws_subnet.global.vpc_id]
  }

  tags = {
    type = "private" // Note the name of the tag is really "type"
  }
}

resource "aws_db_subnet_group" "global" {
  name       = local.identifier
  subnet_ids = data.aws_subnets.global.ids
  tags       = local.tags
}

resource "aws_security_group" "main" {
  name_prefix = "${local.identifier}-db"
  description = "${local.identifier} DB"
  vpc_id      = data.aws_subnet.global.vpc_id

  tags = local.tags

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_security_group_rule" "egress" {
  security_group_id = aws_security_group.main.id

  type        = "egress"
  from_port   = 0
  to_port     = 0
  protocol    = "-1"
  cidr_blocks = ["0.0.0.0/0"]
  description = "Allow access to the internet"
}

resource "aws_security_group_rule" "allow_postgres" {
  count = length(var.allowed_security_groups)
  // Normally I would prefer "for_each" here, but due order of interpolation, on initial deployment,
  // this would throw an error "The "for_each" value depends on resource attributes that cannot be determined", so I used count instead.
  // Truth is, that in this particular case it does not harm in any way, so it's fine.

  security_group_id = aws_security_group.main.id

  type                     = "ingress"
  from_port                = 5432
  to_port                  = 5432
  protocol                 = "tcp"
  source_security_group_id = var.allowed_security_groups[count.index]
  description              = "Postgres port allowed from EKS security group"
}
