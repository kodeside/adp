// Cluster definition with automatic subnet discovery and security groups
// Name of the cluster defined by env name for easy discovery

data "aws_kms_key" "main" {
  key_id = "alias/arbes/EKS"
}

resource "aws_eks_cluster" "main" {
  name     = var.cluster_name
  role_arn = aws_iam_role.eks_cluster.arn

  vpc_config {
    endpoint_public_access = var.public_access_api
    subnet_ids             = var.private_subnet_ids
  }

  encryption_config {
    provider {
      key_arn = data.aws_kms_key.main.arn
    }
    resources = ["secrets"]
  }

  # Ensure that IAM Role permissions are created before and deleted after EKS Cluster handling.
  # Otherwise, EKS will not be able to properly delete EKS managed EC2 infrastructure such as Security Groups.
  depends_on = [
    aws_iam_role_policy_attachment.attach_cluster_policy,
    aws_iam_role_policy_attachment.attach_vpc_controller_policy,
  ]

  tags = local.tags
}

resource "aws_eks_node_group" "main" {
  cluster_name    = aws_eks_cluster.main.name
  node_group_name = "default"
  node_role_arn   = aws_iam_role.eks_worker.arn
  subnet_ids      = var.private_subnet_ids
  disk_size       = 100

  scaling_config {
    desired_size = var.max_size == 0 ? 0 : 1 // This enables to scale to 0 in case of cost savings needed etc. Should be removed before real usage.
    max_size     = var.max_size
    min_size     = var.min_size
  }

  update_config {
    max_unavailable = 1
  }

  # Ensure that IAM Role permissions are created before and deleted after EKS Node Group handling.
  # Otherwise, EKS will not be able to properly delete EC2 Instances and Elastic Network Interfaces.
  depends_on = [
    aws_iam_role_policy_attachment.attach_worker_policy,
    aws_iam_role_policy_attachment.attach_worker_cni_policy,
    aws_iam_role_policy_attachment.attach_worker_registry_access,
  ]

  tags = local.tags
}
