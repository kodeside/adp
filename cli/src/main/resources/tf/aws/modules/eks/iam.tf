data "aws_iam_policy_document" "instance-assume-role-policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com", "eks.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "eks_cluster" {
  name               = "${var.cluster_name}-cluster"
  assume_role_policy = data.aws_iam_policy_document.instance-assume-role-policy.json
}

data "aws_iam_policy" "eks_cluster" {
  name = "AmazonEKSClusterPolicy"
}

data "aws_iam_policy" "eks_vpc_controller" {
  name = "AmazonEKSVPCResourceController"
}

resource "aws_iam_role_policy_attachment" "attach_cluster_policy" {
  role       = aws_iam_role.eks_cluster.name
  policy_arn = data.aws_iam_policy.eks_cluster.arn
}

resource "aws_iam_role_policy_attachment" "attach_vpc_controller_policy" {
  role       = aws_iam_role.eks_cluster.name
  policy_arn = data.aws_iam_policy.eks_vpc_controller.arn
}

resource "aws_iam_role" "eks_worker" {
  name               = "${var.cluster_name}-worker"
  assume_role_policy = data.aws_iam_policy_document.instance-assume-role-policy.json
}

data "aws_iam_policy" "worker_policy" {
  name = "AmazonEKSWorkerNodePolicy"
}

data "aws_iam_policy" "worker_cni" {
  name = "AmazonEKS_CNI_Policy"
}

data "aws_iam_policy" "worker_registry_access" {
  name = "AmazonEC2ContainerRegistryReadOnly"
}

resource "aws_iam_role_policy_attachment" "attach_worker_policy" {
  role       = aws_iam_role.eks_worker.name
  policy_arn = data.aws_iam_policy.worker_policy.arn
}

resource "aws_iam_role_policy_attachment" "attach_worker_cni_policy" {
  role       = aws_iam_role.eks_worker.name
  policy_arn = data.aws_iam_policy.worker_cni.arn
}

resource "aws_iam_role_policy_attachment" "attach_worker_registry_access" {
  role       = aws_iam_role.eks_worker.name
  policy_arn = data.aws_iam_policy.worker_registry_access.arn
}
