output "source_security_group_id" {
  value = aws_eks_cluster.main.vpc_config[0].cluster_security_group_id
}

output "k8s_cluster_name" {
  value = aws_eks_cluster.main.name
}
