// Instance types, amount of nodes, target VPC and subnets
// Custom name identifier

variable "cluster_name" {
  type        = string
  description = "(optional) describe your variable"
}

variable "max_size" {
  type        = number
  default     = 3
  description = "(optional) describe your variable"
}

variable "min_size" {
  type        = number
  default     = 0
  description = "(optional) describe your variable"
}

variable "public_access_api" {
  type        = bool
  default     = false
  description = "(optional) describe your variable"
}

variable "tags" {
  type = map(string)
}

variable "private_subnet_ids" {
  type        = list
  description = "(optional) describe your variable"
}
