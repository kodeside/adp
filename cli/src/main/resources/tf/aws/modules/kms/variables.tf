variable "name" {}

variable "description" {
  default = "Encryption key for the "
}

variable "deletion_window_in_days" {
  default = 30
}

variable "is_enabled" {
  default = true
}

variable "key_usage" {
  description = "Intentoinal usage of the key. Choose between ENCRYPT_DECRYPT and SIGN_VERIFY."
  default     = "ENCRYPT_DECRYPT"
}

variable "key_spec" {
  description = "Specifies whether the key contains a symmetric key or an asymmetric key pair and the encryption algorithms or signing algorithms that the key supports."
  default     = "SYMMETRIC_DEFAULT"
}

variable "grant_via" {
  type        = list(string)
  default     = []
  description = "AWS services to allow to use the key."
}

variable "environment" {
  type        = string
  default     = "production"
  description = "(optional) describe your variable"
}
