# ADP

How to download CLI on Mac/Unix
```
curl https://gitlab.com/api/v4/projects/40915190/repository/files/adp/raw > adp
chmod +x adp
adp -h
```

How to download CLI on Windows
```
curl https://gitlab.com/api/v4/projects/40915190/repository/files/adp.cmd/raw > adp.cmd
adp.cmd -h
```
