terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.37.0"
    }

    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "2.16.0"
    }
  }
}

provider "aws" {
  region  = "eu-central-1"
  profile = "arbes"
}

terraform {

  backend "s3" {
    bucket = "tfstates-arbes-dev"

    workspace_key_prefix = "applications/adp-ri"
    // Using non default workspace, resulted path to the statefile looks following:
    // s3://tfstates-arbes-dev/applications/adp-ri/{terraform.workspace}}/adp-ri-statefile.tfstate
    key    = "adp-ri-statefile.tfstate"
    region = "eu-central-1"
  }

  //experiments = [module_variable_optional_attrs]
}

data "aws_eks_cluster" "main" {
  name = data.terraform_remote_state.global.outputs.k8s_cluster_name
}

data "aws_eks_cluster_auth" "main" {
  name = data.terraform_remote_state.global.outputs.k8s_cluster_name
}

provider "kubernetes" {
  host                   = data.aws_eks_cluster.main.endpoint
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.main.certificate_authority[0].data)
  token                  = data.aws_eks_cluster_auth.main.token
}

# provider "postgres" {
#   alias = "vanilla"
# }

# provider "postgres" {
#   alias = "aurora"
# }
