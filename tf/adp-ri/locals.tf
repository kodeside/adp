locals {
  app_name   = "adp-ri"
  identifier = "${local.app_name}-${terraform.workspace}"
}
