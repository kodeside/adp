# module "application_namespace" {
#   name       = local.deployment_identifier
#   kubernetes = module.kubernetes.cluster_name // Will use K8s provider inside to login to K8s cluster and create namespace
# }

# module "database" {
#   source      = ""
#   master_user = local.deployment_identifier

#   secret_store = local.deployment_identifier // Will use K8s provider 

# }

# module "docker_registry" {
#   source = "value"
#   name = local.deployment_identifier
# }

resource "kubernetes_namespace" "adp_ri" {
  metadata {
    annotations = {
      name = "example-annotation"
    }

    labels = {
      mylabel = "label-value"
    }

    name = local.identifier
  }
}
