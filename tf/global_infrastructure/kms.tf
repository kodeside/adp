// Create KMS keys for encryption

module "kms_eks" {
  source = "../modules/kms"
  name   = "EKS"

  grant_via = [
    "eks.eu-central-1.amazonaws.com",
  ]

}

module "kms_rds" {
  source = "../modules/kms"
  name   = "RDS"

  grant_via = [
    "rds.eu-central-1.amazonaws.com"
  ]
}
