terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.37.0"
    }
  }
}

provider "aws" {
  region  = "eu-central-1"
  profile = "arbes"
}

terraform {

  backend "s3" {
    // Pay attention to this bucket in global organization
    // This bucket must exist prior - typically created manually and never touched afterwards - initial configuration matters!
    // - turn on versioning
    // - encrypt it with KMS in different AWS isolated single purposed AWS account - only for KMS keys
    // - limit who can access it to technical accounts and administrators
    // - keep it in single isolated one-purposed AWS account - only for TF states
    // Below values for foundations development
    bucket = "tfstates-arbes-dev"
    key    = "global_infrastructure/arbes-ri/dev.tfstate"
    region = "eu-central-1"
  }

  //experiments = [module_variable_optional_attrs]
}
