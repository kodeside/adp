locals {

  case = var.use_case_name

  environment = var.environment

  identifier = join("-", [local.case, local.environment])

  azs = [
    "eu-central-1a",
    "eu-central-1b",
    "eu-central-1c",
  ]

}
