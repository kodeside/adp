output "public_subnet_ids" {
  value = module.main_vpc.public_subnet_ids
}

output "private_subnet_ids" {
  value = module.main_vpc.private_subnet_ids
}

output "k8s_cluster_name" {
  value = module.kubernetes.k8s_cluster_name
}
