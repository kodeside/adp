variable "cidr_block" {
  type = string
}

variable "name" {
  type        = string
  description = "Easy to identifiable default name for VPC"
}

variable "environment" {
  type        = string
  description = "Identifier of desired environment"
}

variable "azs" {
  type = list
}

variable "subnet_size" {
  type        = number
  default     = 7
  description = "(optional) describe your variable"
}

variable "subnet_start" {
  type        = number
  default     = 4
  description = "(optional) describe your variable"
}

variable "private_subnets_count" {
  type        = number
  default     = 3
  description = "(optional) describe your variable"
}

variable "public_subnets_count" {
  type        = number
  default     = 3
  description = "(optional) describe your variable"
}

variable "tags" {
  type        = map(string)
  description = "(optional) describe your variable"
}
