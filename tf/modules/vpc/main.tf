// VPC
resource "aws_vpc" "main" {
  cidr_block = var.cidr_block

  tags = merge({
    Name = var.name,
    },
    var.tags
  )
}

// Public subnets

resource "aws_subnet" "public" {
  count  = length(var.azs)
  vpc_id = aws_vpc.main.id
  cidr_block = cidrsubnet(
    var.cidr_block,
    var.subnet_size,
    count.index + var.subnet_start
  )
  availability_zone       = var.azs[count.index]
  map_public_ip_on_launch = true

  tags = merge({
    Name = "Terraform Public ${count.index}",
    type = "public"
    },
    var.tags
  )
}

// Private subnets

resource "aws_subnet" "private" {
  count  = length(var.azs)
  vpc_id = aws_vpc.main.id
  cidr_block = cidrsubnet(
    var.cidr_block,
    var.subnet_size,
    count.index + var.subnet_start + length(var.azs),
  )
  availability_zone       = var.azs[count.index]
  map_public_ip_on_launch = false

  tags = merge({
    Name = "Terraform Private ${count.index}",
    type = "private"
    },
    var.tags
  )
}
