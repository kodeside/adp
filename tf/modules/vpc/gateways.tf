resource "aws_eip" "nat" {
  count = length(var.azs)
  vpc   = true
}

resource "aws_nat_gateway" "main" {
  count         = length(var.azs)
  allocation_id = element(aws_eip.nat.*.id, count.index)
  subnet_id     = element(aws_subnet.public.*.id, count.index)

  depends_on = [aws_internet_gateway.main]
}

resource "aws_internet_gateway" "main" {
  tags = merge({
    Name = "Main IGW",
    },
    var.tags
  )
}

resource "aws_internet_gateway_attachment" "example" {
  internet_gateway_id = aws_internet_gateway.main.id
  vpc_id              = aws_vpc.main.id
}
