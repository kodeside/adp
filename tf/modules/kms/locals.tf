locals {
  description = "${var.description}${var.name}"

  rotation_enabled = var.key_usage == "ENCRYPT_DECRYPT"

  tags = merge({ Name = var.name },
  module.kms_tags.tags)

  crypto_actions = {
    ENCRYPT_DECRYPT = [
      "kms:Encrypt",
      "kms:Decrypt",
      "kms:ReEncrypt*",
      "kms:GenerateDataKey*",
      "kms:DescribeKey",
    ],
    SIGN_VERIFY = [
      "kms:DescribeKey",
      "kms:GetPublicKey",
      "kms:Sign",
    ],
  }
}
