// KMS keys should be stored in single isolated AWS account granting access to it from different accounts
// Here we simply deploy keys ONCE - in case multiple databases and/or Kubernetes clusters are deployed,
// those will find KMS key based on the name

data "aws_caller_identity" "current" {}

// Tags

module "kms_tags" {
  source              = "../tags"
  environment         = var.environment // KMS keys are high level resources, considered merely _always_ as production ones
  git_repository      = "gitlab.com/arbes-technologies/adp-ri"
  data_classification = "confidential"
}

// Main key definition
resource "aws_kms_key" "main" {
  description              = local.description
  deletion_window_in_days  = var.deletion_window_in_days
  enable_key_rotation      = local.rotation_enabled
  is_enabled               = var.is_enabled
  key_usage                = var.key_usage
  customer_master_key_spec = var.key_spec
  policy                   = data.aws_iam_policy_document.main.json
  tags                     = local.tags
  multi_region             = true
}

resource "aws_kms_alias" "main" {
  name          = "alias/arbes/${var.name}"
  target_key_id = aws_kms_key.main.key_id
}

data "aws_iam_policy_document" "main" {
  source_policy_documents = concat(
    [data.aws_iam_policy_document.default.json],
    length(var.grant_via) == 0 ? [] : [data.aws_iam_policy_document.grants.json],
  length(var.grant_via) == 0 ? [data.aws_iam_policy_document.grants-override.json] : [])
}

// Key policies
data "aws_iam_policy_document" "default" {
  version = "2012-10-17"

  statement {
    sid    = "Enable IAM User Permissions"
    effect = "Allow"
    principals {
      type        = "AWS"
      identifiers = ["arn:aws:iam::${data.aws_caller_identity.current.account_id}:root"]
    }
    actions = [
      "kms:*"
    ]
    resources = ["*"]
  }
}

data "aws_iam_policy_document" "grants" {
  statement {
    sid    = "Allow attachment of persistent resources"
    effect = "Allow"
    principals {
      type        = "AWS"
      identifiers = ["arn:aws:iam::${data.aws_caller_identity.current.account_id}:root"]
    }
    actions = [
      "kms:CreateGrant",
      "kms:ListGrants",
      "kms:RevokeGrant"
    ]
    resources = ["*"]

    condition {
      test     = "StringEquals"
      variable = "kms:ViaService"
      values   = var.grant_via
    }
  }
}

data "aws_iam_policy_document" "grants-override" {
  statement {
    sid    = "Allow attachment of persistent resources"
    effect = "Allow"
    principals {
      type        = "AWS"
      identifiers = ["arn:aws:iam::${data.aws_caller_identity.current.account_id}:root"]
    }
    actions = [
      "kms:CreateGrant",
      "kms:ListGrants",
      "kms:RevokeGrant"
    ]
    resources = ["*"]
  }
}

# This will become VERY handy, once real KMS will de deployed in isolated AWS account
#   statement {
#     sid    = "Allow external accounts to use this CMK"
#     effect = "Allow"
#     principals {
#       type        = "AWS"
#       identifiers = [for i, v in var.accounts : "arn:aws:iam::${local.account_ids[v]}:root"]
#     }
#     actions   = local.crypto_actions[var.key_usage]
#     resources = ["*"]

#     condition {
#       test     = "StringEquals"
#       variable = "kms:CallerAccount"
#       values   = [for i, v in var.accounts : local.account_ids[v]]
#     }
#   }
# }

# data "aws_iam_policy_document" "grants-override" {
#   statement {
#     sid    = "Allow attachment of persistent resources"
#     effect = "Allow"
#     principals {
#       type        = "AWS"
#       identifiers = [for i, v in var.accounts : "arn:aws:iam::${local.account_ids[v]}:root"]
#     }
#     actions = [
#       "kms:CreateGrant",
#       "kms:ListGrants",
#       "kms:RevokeGrant"
#     ]
#     resources = ["*"]
#   }
# }
