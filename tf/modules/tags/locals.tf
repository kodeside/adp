locals {
  environment_values = {
    "dev"        = "dev"
    "uat"        = "uat"
    "production" = "production"
    "global"     = "global"
  }

  data_classification_values = {
    "public"       = "public"
    "internal"     = "internal"
    "sensitive"    = "sensitive"
    "confidential" = "confidential"
  }

  optional_tags = {
    Name = var.name
  }

  mandatory_tags = {
    environment         = local.environment_values[var.environment]
    data_classification = local.data_classification_values[var.data_classification]
    git_repository      = var.git_repository
  }

  tags = merge(
    { for key, value in local.optional_tags : key => value if value != "" },
    local.mandatory_tags,
    var.custom_tags
  )
}
