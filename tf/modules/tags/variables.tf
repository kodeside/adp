variable "name" {
  type        = string
  description = "Resource name"
  default     = ""
}

variable "environment" {
  type        = string
  description = "An enviroment the resource is going to live in"
}

variable "git_repository" {
  type        = string
  description = "Name of repository hosting your terraform code"
}

variable "data_classification" {
  type        = string
  description = "Data classification tag"
}

variable "custom_tags" {
  type        = map(string)
  description = "Additional tags you want to label resources with. You can also use it to override some of the defaults (a Name tag for example)"
  default     = {}
}
