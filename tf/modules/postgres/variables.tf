// Variables for instance types and storage class
variable "identifier" {
  type        = string
  description = "Identifier of database host."
}

variable "username" {
  type        = string
  default     = "master"
  description = "Desired master username"
}

variable "db_instance_type" {
  type        = string
  default     = "db.t3.medium"
  description = "(optional) describe your variable"
}

variable "auto_minor_version_upgrade" {
  type        = bool
  description = "Value for  auto_minor_version_upgrade(optional)"
  default     = false
}

variable "log_error_verbosity" {
  type        = string
  description = "(optional) describe your variable"
  default     = null
}
variable "max_connections" {
  type        = string
  description = "Maximum connections allowed to this RDS instance. (optional)"
  default     = "default"
}

variable "log_min_duration_statement" {
  type        = string
  description = "Value for log_min_duration_statement. (optional)"
  default     = "5000"
}

variable "enabled_cloudwatch_logs_exports" {
  type        = list(string)
  description = "Value of (Optional) Set of log types to enable for exporting to CloudWatch logs"
  default     = ["postgresql", "upgrade"]
}

variable "performance_insights_enabled" {
  type        = string
  description = "Value of (Optional) Specifies whether Performance Insights are enabled. Defaults to false"
  default     = "true"
}

variable "log_connections" {
  type        = string
  description = "Value of log_connections"
  default     = "1"
}

variable "storage_size" {
  type        = number
  default     = 100
  description = "(optional) describe your variable"
}

variable "postgres_version" {
  type        = string
  default     = "13.7"
  description = "(optional) describe your variable"
}

variable "multi_az" {
  type        = bool
  default     = false
  description = "(optional) describe your variable"
}

variable "log_disconnections" {
  type        = string
  description = "Value of log_disconnections"
  default     = "1"
}

variable "log_statement" {
  type        = string
  description = "Value of log_statement ddl or mod, or all"
  default     = "none"
}

variable "param_group_family" {
  type        = string
  description = "Value for db parameter group. This should be updated when upgrading major versions. (optional)"
  default     = "postgres13"
}

variable "shared_preload_libraries" {
  description = "mapping for shared_preload_libraries param for engine version"
  default = {
    "14" = ["pg_stat_statements", "pgaudit", "pg_cron"]
    "13" = ["pg_stat_statements", "pgaudit", "pg_cron"]
  }
}

variable "pgaudit_role" {
  type        = string
  description = "Specifies the master role to use for object audit logging"
  default     = "rds_pgaudit"
}

variable "tags" {
  type = map(string)
}

variable "deletion_protection" {
  type    = bool
  default = false
}

variable "allowed_security_groups" {
  type = list
}
