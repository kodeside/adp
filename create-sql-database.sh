#!/bin/bash
kubectl -n $1 exec deploy/postgres -c database -- sh -c "echo \"SELECT 'CREATE DATABASE $2' WHERE NOT EXISTS (SELECT FROM pg_database WHERE datname = '$2')\gexec\" | psql -U postgres"