#!/bin/sh
if [ -z "$ADP_DIR" ]
then
  ADP_DIR=/tmp
fi

if [ -z "$ADP_CLI_JAR" ]
then
  ADP_CLI_JAR=adp-cli-latest.jar
fi

if [ ! -f $ADP_DIR/$ADP_CLI_JAR ]
then
  curl https://arbes-repository.s3.eu-central-1.amazonaws.com/dist/adp-cli/$ADP_CLI_JAR > $ADP_DIR/$ADP_CLI_JAR
fi

export AWS_PROFILE=arbes

java -jar $ADP_DIR/$ADP_CLI_JAR $@